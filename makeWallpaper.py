import sys
import random
import json
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import os
from os import path
import textwrap


# load random wallpaper
mypath = "./images/"
wallpapers = [f for f in os.listdir(mypath) if path.isfile(path.join(mypath, f))]
wallpaperFile = random.choice(wallpapers)
wallpaper = Image.open(path.join(mypath, wallpaperFile))

wallpaper = wallpaper.resize((1920, 1080), Image.ANTIALIAS)


# load random text
json_file = open("posts.json").read()
randnum = random.randint(0,99)
response = json.loads(json_file)["data"]["children"][randnum]["data"]


# draw deep quote to image
mypath = "./fonts/"
fonts = [f for f in os.listdir(mypath) if path.isfile(path.join(mypath, f))]
fontname = random.choice(fonts)
print(fontname)
font = ImageFont.truetype(path.join(mypath, fontname), 40)
draw = ImageDraw.Draw(wallpaper)

text = response["title"]
lines = textwrap.wrap(text)

(wallpaper_x, wallpaper_y) = wallpaper.size
print(wallpaper.size)
pos_y = 100
pos_x = wallpaper_x/2



def drawCenteredText(draw, line, pos_x, pos_y):
    width, height = font.getsize(line)
    draw.text((pos_x - (width/2)+2, pos_y+2), line, font=font, fill=(0,0,0))
    draw.text((pos_x - (width/2), pos_y), line, font=font, fill=(255,255,255))

y_text  = 0
for line in lines:
    width, height = font.getsize(line)
    y_text += height
    drawCenteredText(draw, line, pos_x, (pos_y + y_text))

authorOffset = 100

drawCenteredText(draw, "-   " + response["author"], pos_x, (pos_y + y_text + authorOffset))

wallpaper.save("./wallpaper.png", "PNG")
