import sys
import random
import json
from urllib.request import *
import time
import magic
import mimetypes
import os



# load random text
json_file = open("images.json").read()
children = json.loads(json_file)["data"]["children"]


for child in children:
    url = child["data"]["url"]
    try:
        filename = "images/"+(str(int(time.time()*1000)))
        urlretrieve(url, filename)
        filetype = magic.from_file(filename, mime=True)
        print(filetype)
        ext = mimetypes.guess_extension(filetype) # Guess extension
        print(ext)
        if ext in [".jpg", ".jpeg", ".png", ".jpe"]:
            print(f"Kept {filename}.")
            os.rename(filename, filename+ext) # Rename file
            filename = filename+ext
        else:
            print(f"Removed {filename}.")
            os.remove(filename)
    except Exception as e:
        #print(e)
        try:
            os.remove(filename)
        except Exception as e:
            #print(e)
            pass

