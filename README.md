# Installation
Clone the repository and execute the following scripts:
- `sh fetch.sh` to fetch the latest /r/earthporn and /r/showerthoughts posts.
- `python downloadImages.py` to download the images from /r/eartporn after fetching
- `python makeWallpaper.py` to generate a wallpaper.

`lock` is an example of how this script can be used as a lockscreen with i3wm.
